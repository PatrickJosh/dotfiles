" The background color is not set correctly if vim is used in tmux because
" tmux does not answer \e]11;?\a.
" Therefore I introduce keybindings for changing the background color.

map <F2> :set background=light<CR>
map <F3> :set background=dark<CR>

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

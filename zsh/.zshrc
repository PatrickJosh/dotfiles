# Set language
export LC_ALL="en_US.utf8"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


# Zinit home directory
declare -A ZINIT
ZINIT[HOME_DIR]="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit"
ZINIT[BIN_DIR]="${ZINIT[HOME_DIR]}/zinit.git"
ZINIT[NO_ALIASES]=1

# Download and install zinit if not existent
[ ! -d ${ZINIT[BIN_DIR]} ] && mkdir -p "$(dirname ${ZINIT[BIN_DIR]})"
[ ! -d ${ZINIT[BIN_DIR]}/.git ] && git clone https://github.com/zdharma-continuum/zinit.git "${ZINIT[BIN_DIR]}"

# Source zinit
source "${ZINIT[BIN_DIR]}/zinit.zsh"

# Theme
zinit ice depth=1; zinit light romkatv/powerlevel10k

# Add custom plugins
zinit light Aloxaf/fzf-tab
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions

# Add oh-my-zsh plugins
zinit snippet OMZL::key-bindings.zsh
zinit snippet OMZL::grep.zsh
zinit snippet OMZP::git
zinit snippet OMZP::gitignore
zinit snippet OMZP::tmux
zinit snippet OMZP::command-not-found

# Load completions
autoload -Uz compinit && compinit
zinit cdreplay -q

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

### Completion
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|=*' 'l:|=* r:|=*'
# force zsh not to show completion menu, which allows fzf-tab to capture the unambiguous prefix
zstyle ':completion:*' menu no
setopt correct

### Keybindings
bindkey -e
bindkey '^Q' push-input
bindkey '^[q' push-input

### History
HISTFILE="$HOME/.zsh_history"
HISTSIZE=100000
SAVEHIST=$HISTSIZE
setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_reduce_blanks     # remove superfluous blanks from command before adding it to history
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt share_history          # share command history data

### zsh-autosuggest
ZSH_AUTOSUGGEST_STRATEGY="match_prev_cmd"
bindkey '^ ' autosuggest-accept

### fzf and fzf-tab
FZF_CTRL_T_COMMAND="fd --xdev --follow --unrestricted --exclude .git"
FZF_ALT_C_COMMAND="fd --type d --xdev --follow --unrestricted --exclude .git"
FZF_ALT_SHIFT_C_COMMAND="$FZF_CTRL_T_COMMAND"
FZF_DEFAULT_OPTS="--bind='tab:down,btab:up,change:top,ctrl-space:toggle,bspace:backward-delete-char/eof,ctrl-h:backward-delete-char/eof'"
FZF_CTRL_T_OPTS="--preview 'if [[ -d {} ]]; then eza --color=always {}; else eza -lg --color=always {}; file {}; fi'"
FZF_ALT_C_OPTS="--preview 'eza --color=always {}'"
FZF_ALT_SHIFT_C_OPTS="--preview 'eza --color=always \"`dirname {}`\"'"
_fzf_compgen_path() {
    fd --xdev --follow --unrestricted --exclude ".git" . "$1"
}
_fzf_compgen_dir() {
    fd --type d --xdev --follow --unrestricted --exclude ".git" . "$1"
}
_fzf_comprun() {
    local command=$1
    shift

    case "$command" in
        cd)           fzf ${(Q)${(z)FZF_ALT_C_OPTS}} "$@" ;;
        *)            fzf ${(Q)${(z)FZF_CTRL_T_OPTS}} "$@" ;;
    esac
}

# preview directory's content with eza when completing cd
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza --color=always $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'eza --color=always $realpath'
# preview git diff
zstyle ':fzf-tab:complete:git-diff:*' fzf-preview 'git diff --compact-summary $word'
# Use tmux popup
#zstyle ':fzf-tab:*' fzf-command ftb-tmux-popup
# Currently not as there's a problem with the width of the popup

### zoxide
## interactive widget and keybinding
zi_widget() {
    \builtin local dir
    dir="$(\command zoxide query --interactive -- "$@")"
    if [[ -z "$dir" ]]
	then
		zle redisplay
		return 0
	fi
	zle push-line
	BUFFER="builtin cd -- ${(q)dir:a}"
	zle accept-line
	local ret=$?
	unset dir
	zle reset-prompt
	return $ret
}
zle -N zi_widget
bindkey "v" zi_widget

fzf-cd-dirname-widget() {
  setopt localoptions pipefail no_aliases 2> /dev/null
  local dir="$(
    FZF_DEFAULT_COMMAND=${FZF_ALT_SHIFT_C_COMMAND:-} \
    FZF_DEFAULT_OPTS=$(__fzf_defaults "--reverse --walker=dir,follow,hidden --scheme=path" "${FZF_ALT_SHIFT_C_OPTS-} +m") \
    FZF_DEFAULT_OPTS_FILE='' $(__fzfcmd) < /dev/tty)"
  if [[ -z "$dir" ]]; then
    zle redisplay
    return 0
  fi
  zle push-line # Clear buffer. Auto-restored on next prompt.
  if [[ -d "${dir:a}" ]]; then
      BUFFER="builtin cd -- ${(q)dir:a}"
  else
      BUFFER="builtin cd -- `dirname ${(q)dir:a}`"
  fi
  zle accept-line
  local ret=$?
  unset dir # ensure this doesn't end up appearing in prompt expansion
  zle reset-prompt
  return $ret
}
zle -N fzf-cd-dirname-widget
bindkey "^[C" fzf-cd-dirname-widget

### thefuck
# eval $(thefuck --alias)

### Diff
diffc () {
    diff --color=always -u $1 $2 | less -R
}

### eza
alias e='eza'
alias ll='eza -lag'
alias l='ll --git'
alias lZ="l -Z"

### Editor
export EDITOR="/usr/bin/vim"

### Directory changing
setopt auto_pushd
setopt pushd_minus

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

alias 1='cd -1'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'

alias d='dirs -v | head -n 10'

##### End of main config


##### Aftermath
# For stuff that has to be at the end

### Init fzf
eval "$(fzf --zsh)"

### Init zoxide
eval "$(zoxide init zsh)"
